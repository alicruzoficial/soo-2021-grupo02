package soo2021grupo02.soo2021grupo02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Soo2021Grupo02Application {

	public static void main(String[] args) {
		SpringApplication.run(Soo2021Grupo02Application.class, args);
	}

}
