package soo2021grupo02.soo2021grupo02.controller;

import org.springframework.web.bind.annotation.RestController;

import soo2021grupo02.soo2021grupo02.service.MedicoService;
import soo2021grupo02.soo2021grupo02.service.dto.MedicoDTO;

import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@RequestMapping("/api/medico")
public class MedicoResource {
//definir las rutas donde se va a encontrar la api 
	private final Logger log = LoggerFactory.getLogger(MedicoResource.class);
	
	@Autowired
	MedicoService medicoservice;
	//devolver todos los medicos
	@GetMapping("/medicos")
	public List<MedicoDTO> getAllMedico(){
		log.debug("API REST buscando todos los Medicos");
		return medicoservice.getAllMedico();
	}
	//buscar por id
	@GetMapping("/medicos/{id}")
	public MedicoDTO getMedico(@PathVariable Long id) {
		log.debug("API REST buscando EL empleado con el id =  [" + id + "]");
		return medicoservice.getMedico(id);	
	}
	//busca por matricula
	@GetMapping("/medicos/matricula/{matricula}")
	public MedicoDTO getMedicoMatri(@PathVariable String matricula) {
		log.debug("API REST buscando EL empleado con el id =  [" + matricula + "]");
		return medicoservice.getMedicoMatri(matricula);	
	}
	//Agregar un nuevo medico
	@PostMapping("/medicos")
    public MedicoDTO addMedico(@RequestBody MedicoDTO medicodto) {
        //Este metodo guardará al usuario enviado
		medicoservice.addMedico(medicodto.getNombre(), medicodto.getMatricula());

        return medicodto;

    }
	
	//Eliminar un medico 
	@DeleteMapping("/medicos/delete/{matricula}")
	public void deleteMedico(@PathVariable String matricula) {
	    //Este metodo guardará al usuario enviado
		medicoservice.deleteMedico(matricula);

	}
	//eliminar por id un medico
	@DeleteMapping("/medicos/{id}")
	public void deleteMedicoId(@PathVariable long id) {
	    //Este metodo guardará al usuario enviado
		medicoservice.deleteMedicoId(id);

	}
	
}
