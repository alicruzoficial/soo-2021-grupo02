package soo2021grupo02.soo2021grupo02.service.dto;

import java.io.Serializable;


import com.fasterxml.jackson.annotation.JsonProperty;
public class MedicoDTO implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "id")
	private Long id;
	
	@JsonProperty(value = "nombre")
    private String nombre;

	@JsonProperty(value = "matricula")
    private String matricula;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
}
