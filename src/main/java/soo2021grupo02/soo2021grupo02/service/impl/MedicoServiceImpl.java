package soo2021grupo02.soo2021grupo02.service.impl;

import java.util.ArrayList;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import soo2021grupo02.soo2021grupo02.entity.Medico;
import soo2021grupo02.soo2021grupo02.repository.MedicoRepository;
import soo2021grupo02.soo2021grupo02.service.MedicoService;
import soo2021grupo02.soo2021grupo02.service.dto.MedicoDTO;

@Service
public class MedicoServiceImpl implements MedicoService{
	
	@Autowired
	private MedicoRepository medicoRepository;
	
	
	@Override
	public List<MedicoDTO> getAllMedico() {
		// TODO Auto-generated method stub
		List<Medico> medicos = medicoRepository.findAll();
		List<MedicoDTO> dtos = new ArrayList<MedicoDTO>();
		for(Medico m :medicos) {
			MedicoDTO dto = new MedicoDTO();
			//lo que hago es leer del repository y tranformarlo en  json
			dto.setId(m.getId());
			dto.setNombre(m.getNombre());
			dto.setMatricula(m.getMatricula());
			
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public void addMedico(String nombre, String matricula) {
		// TODO Auto-generated method stub
		Medico newmedico = new Medico();
		newmedico.setNombre(nombre);
		newmedico.setMatricula(matricula);
		medicoRepository.save(newmedico);
	}

	@Override
	public MedicoDTO getMedico(long id) {
		Optional<Medico> medicobus = medicoRepository.findById(id);
		MedicoDTO dto = new MedicoDTO();
		dto.setId(medicobus.get().getId());
		dto.setNombre(medicobus.get().getNombre());
		dto.setMatricula(medicobus.get().getMatricula());
	
		return dto;
	}

	@Override
	public MedicoDTO getMedicoMatri(String matricula) {
		// TODO Auto-generated method stub
		
		Medico medicobus = medicoRepository.findByMatricula(matricula);
		MedicoDTO dto = new MedicoDTO();
		dto.setId(medicobus.getId());
		dto.setNombre(medicobus.getNombre());
		dto.setMatricula(medicobus.getMatricula());
	
		return dto;
	}
	
	@Override
	public void deleteMedico(String matricula) {
		// TODO Auto-generated method stub
		medicoRepository.deleteByMatricula(matricula);
	}

	@Override
	public void deleteMedicoId(long id) {
		// TODO Auto-generated method stub
		medicoRepository.deleteById(id);
	}
	

}
