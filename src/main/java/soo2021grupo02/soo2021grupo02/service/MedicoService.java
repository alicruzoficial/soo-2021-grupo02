package soo2021grupo02.soo2021grupo02.service;
import java.util.List;

import soo2021grupo02.soo2021grupo02.service.dto.MedicoDTO;
public interface MedicoService {
	
	List<MedicoDTO> getAllMedico();
	
	void addMedico(String nombre, String matricula);
	
	void deleteMedico(String matricula);
	
	void deleteMedicoId(long id);
	
	MedicoDTO getMedico(long id);
	
	MedicoDTO getMedicoMatri(String matricula);
	
}
