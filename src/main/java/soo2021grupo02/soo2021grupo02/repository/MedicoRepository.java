package soo2021grupo02.soo2021grupo02.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import soo2021grupo02.soo2021grupo02.entity.Medico;

@Repository
public interface MedicoRepository extends CrudRepository<Medico ,Long>{
	public Medico findByMatricula(String matricula);
	public Medico findByNombre(String nombre);
	public List<Medico> findAll();
	public void deleteByMatricula(String matricula);
}
