package soo2021grupo02.soo2021grupo02.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import soo2021grupo02.soo2021grupo02.entity.Paciente;

@Repository
public interface PacienteRepository extends CrudRepository<Paciente ,Long>{
	public Paciente findByDni(Integer dni);
}