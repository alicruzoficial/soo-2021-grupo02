package soo2021grupo02.soo2021grupo02;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

import soo2021grupo02.soo2021grupo02.entity.Paciente;
import soo2021grupo02.soo2021grupo02.repository.PacienteRepository;

@SpringBootTest
@WebAppConfiguration
public class PacienteTest {

	@Autowired
	PacienteRepository repo;
	
	@BeforeAll
	void AddPacientes() {
		Paciente paciente1 = new Paciente();
		paciente1.setDni(111111);
		paciente1.setNombre("Iara");
		repo.save(paciente1);
		
		Paciente paciente2 = new Paciente();
		paciente2.setDni(222222);
		paciente2.setNombre("Lucy");
		repo.save(paciente2);
	}
	
	@AfterAll
	void RemovePacientes() {
		repo.deleteAll();
	}
	
	@Test
	void createTest() {
		Paciente paciente = new Paciente();
		paciente.setDni(2345678);
		paciente.setNombre("Elizabeth");
		Paciente pacienteSaved = repo.save(paciente);
		assertNotNull(pacienteSaved);
	}
	
	@Test
	void getPacienteByDni() {
		Integer dni = 111111;
		Paciente pacienteFound = repo.findByDni(dni);
		assertNotNull(pacienteFound);
	}
	
	@Test
	void getListPacientes() {
		List<Paciente> pacientes = (List<Paciente>) repo.findAll();
		assertEquals(pacientes.size(), 2);
	}
	
	@Test
	void deletePaciente() {
		Integer dni = 111111;
		Paciente pacienteFound = repo.findByDni(dni);
		repo.delete(pacienteFound);
	}
}
