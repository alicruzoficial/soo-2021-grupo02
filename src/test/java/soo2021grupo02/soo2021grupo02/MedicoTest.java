package soo2021grupo02.soo2021grupo02;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import soo2021grupo02.soo2021grupo02.entity.Medico;
import soo2021grupo02.soo2021grupo02.repository.MedicoRepository;



@SpringBootTest
public class MedicoTest {
	@SuppressWarnings("deprecation")
	static Medico  medi;
	@Autowired
	MedicoRepository medicorepository;	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {}
	@AfterAll
	static void tearDownAfterClass() throws Exception {}
	@BeforeEach
	void setUp() throws Exception {
		System.out.println("Configurando en SetUp...");
		//creando objeto
		medi = new Medico();
		medi.setNombre("Diego");
		medi.setMatricula("MC23");
	}
	
	@AfterEach
	void tearDown() throws Exception {
		System.out.println("Limpiando variables...K");
		//limpiando variables
		medi=null; 
		medicorepository.deleteAll();
		
	}
	@Test
	@DisplayName("Registrar un Medico")
	void testSaveMedico() {
		//persistiendo
		medicorepository.save(medi);
		//buscando si el archivo fue persistido
		Boolean valor = medicorepository.existsById(medi.getId());
		assertTrue(valor);
		//eliminando objeto
		medicorepository.deleteById(medi.getId());
	}
	@Test
	@DisplayName("Modificar un Medico")
	void testDeleteMedico() {
		//persistiendo
		medicorepository.save(medi);
		//buscando si el archivo fue persistido
		Boolean valor = medicorepository.existsById(medi.getId());
		assertTrue(valor);
		//modificar el nombre
		medi.setNombre("Ali");
		//persistiendo
		medicorepository.save(medi);
		//buscando
		Optional<Medico> medico = medicorepository.findById(medi.getId());
		//comparando
		assertEquals(medico.get().getId(), medi.getId()); 
		//eliminando
		medicorepository.deleteById(medi.getId());
	}
	@Test
	@DisplayName("Buscar por Matricula un Medico")
	void testBuscarMatriculaMedico() {
		//persistiendo
		medicorepository.save(medi);
		//buscando si el archivo fue persistido por matricula
		Medico valor = medicorepository.findByMatricula("MC23");
		assertNotNull(valor);
		//eliminando objeto
		medicorepository.deleteById(medi.getId());
	}
	@Test
	@DisplayName("Buscar por Nombre un Medico")
	void testBuscarNombreMedico() {
		//persistiendo
		medicorepository.save(medi);
		//buscando si el archivo fue persistido por nombre
		Medico valor = medicorepository.findByNombre("Diego");
		assertNotNull(valor);
		//eliminando objeto
		medicorepository.deleteById(medi.getId());
	}
}
