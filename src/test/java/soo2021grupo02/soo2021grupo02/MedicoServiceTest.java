package soo2021grupo02.soo2021grupo02;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import soo2021grupo02.soo2021grupo02.entity.Medico;
import soo2021grupo02.soo2021grupo02.service.MedicoService;
import soo2021grupo02.soo2021grupo02.service.dto.MedicoDTO;

@SpringBootTest
public class MedicoServiceTest {
	@SuppressWarnings("deprecation")
	static Medico  medi;
	static MedicoDTO  medidto;
	@Autowired
	MedicoService medicoservice;	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {}
	@AfterAll
	static void tearDownAfterClass() throws Exception {}
	@BeforeEach
	void setUp() throws Exception {
		System.out.println("Configurando en SetUp...");
		//creando objeto
		medi = new Medico();
		medi.setNombre("Diego");
		medi.setMatricula("MC23");
		//pasando a Dto ,osea convertirlos a json 
		medidto=new MedicoDTO();
		medidto.setNombre(medi.getNombre());
		medidto.setMatricula(medi.getMatricula());
	}
	
	@AfterEach
	void tearDown() throws Exception {
		System.out.println("Limpiando variables...K");
		//limpiando variables
		medi=null; 
		medidto=null;
	}
	
	@Test
	@DisplayName("Registrar un Medico")
	void testSaveMedico() {
		//persistiendo
		medicoservice.addMedico(medidto.getNombre(), medidto.getMatricula());
		//buscando si el archivo fue persistido
		MedicoDTO valor = medicoservice.getMedicoMatri(medidto.getMatricula());
		assertNotNull(valor);
		//eliminando objeto
		medicoservice.deleteMedicoId(valor .getId());
	}
	
}
